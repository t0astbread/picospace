pico-8 cartridge // http://www.pico-8.com
version 18
__lua__
-- vector functions

function v2(x,y)
	return {x=x,y=y}
end

function v2_rot(r)
	return v2(sin(r),cos(r))
end

function v2_add(a,b)
	return v2(
		a.x+b.x,
		a.y+b.y
	)
end

function v2_sub(a,b)
	return v2(
		a.x-b.x,
		a.y-b.y
	)
end

function v2_mul(vec,n)
	return v2(
		vec.x*n,
		vec.y*n
	)
end

function v2_mag(vec)
  return sqrt(
			vec.x*vec.x+
			vec.y*vec.y
  )
end

function v2_norm(vec)
	return v2_mul(
		vec,
		1/v2_mag(vec)
	)
end

function v2_clamp(vec,n)
	return v2_mul(
		v2_norm(vec),
		min(v2_mag(vec),n)
	)
end

function v2_mod(vec,n)
	return v2(
		vec.x%n,
		vec.y%n
	)
end
-->8
-- general

// constants

accel=.65

// variables

player={
	pos=v2(10,10),
	vel=v2(0,0)
}
dead=false
health=100
energy=100

// coroutines run during the main loop
loop_cos={}

function _update()
	if(health<=0 and btnp(5)) extcmd("reset")
	
	// execute and pop coroutines
	for co in all(loop_cos) do
		if costatus(co) then
			coresume(co)
		else
			del(loop_cos,co)
		end
	end
	
	if(health<=0) return

	// accelerate player
	if (btn(0)) player.vel.x-=accel
	if (btn(1)) player.vel.x+=accel
	if (btn(2)) player.vel.y-=accel
	if (btn(3)) player.vel.y+=accel
	
	// slow down & move player
	player.vel=v2_clamp(
		v2_mul(player.vel,.9),
		6
	)
	player.pos=v2_mod(v2_add(
		player.pos,
		player.vel
	),128)
	
	// refill energy
	if(energy<100) energy+=.1

	// spawn player bullet stream
	if(btn(4)) spawn_player_bullet(0)
	
	// spawn player bullet burst
	if(btnp(5)) then
		spawn_player_bullet_burst()
	end

	if(time()%.5==0) then
		spawn_rnd_bullet(0,1)
		spawn_rnd_bullet(128,-1)
	end
	if(time()%10==0) spawn_enemy(0,v2(rnd(100)+14,0))

	update_enemies()
	update_bullets()
	update_player_bullets()
	
	update_dust_particles()
end

function screenshake(int,dur)
	add(loop_cos,
		cocreate(function()
			for i=0,dur do
				camera(rnd(int),rnd(int))
				yield()
			end
			camera(0,0)
		end)
	)
end

function play_hit_fx()
	screenshake(6,1)
	sfx(2,1)
end

function _draw()
	cls(1)

	foreach(bullets,draw_bullet)
	foreach(enemies,draw_enemy)
	foreach(player_bullets,draw_player_bullet)
	foreach(dust_parts,draw_dust_part)
	
	draw_actor(player,1,7)
	
	line(-10,119,138,119,13)
	rectfill(-10,120,138,138,0)
	progbar(
		health/100,
		57,121,
		87,127,
		12
	)
	icon("♥",51,121)
	progbar(
		energy/100,
		96,121,
		126,127,
		11
	)
	icon("◆",90,121)
	
	if(health<=0) print("dead",57,56,7)
end

function draw_actor(actor,size,col)
		for x_off=-1,1 do
			for y_off=-1,1 do
				circfill(
					actor.pos.x
						-size/2
						+x_off*128,
					actor.pos.y
						-size/2
						+y_off*128,
				size,col)
			end
		end
end

function icon(str,x,y)
	rectfill(x,y,x+6,y+6,6)
	print(str,x,y+1,1)
	pset(x,y,0)
	pset(x,y+6,0)
end

function progbar(prog,x1,y1,x2,y2,col)
	rectfill(
		x1,y1,
		x1+(x2-x1)*prog,y2,
		col
	)
	rect(x1,y1,x2,y2,6)
	pset(x1,y1,0)
	pset(x1,y2,0)
	pset(x2,y1,0)
	pset(x2,y2,0)
end
-->8
-- bullets

bullets={}
player_bullets={}

function update_bullets()
	for bullet in all(bullets) do
		// slow down & move bullet
		bullet.vel=v2_mul(
			bullet.vel,
			.985
		)
		bullet.pos=v2_mod(v2_add(
			bullet.pos,
			bullet.vel
		),128)

		// check for collision with player bullet
		for pbullet in all(player_bullets) do
			if(v2_mag(v2_sub(
				pbullet.pos,
				bullet.pos
			)) < 4) then
				del(player_bullets,pbullet)
				bullet.health-=50
				if(bullet.health<=0) bullet.death_part_mul=1
				screenshake(3,3)
			end
		end

		// starve resting bullets
		bullet.starving=v2_mag(bullet.vel)<.05
		if(bullet.starving) bullet.health-=1

		// remove the bullet if it's off-screen
		if(bullet.health <= 0) then
			del(bullets,bullet)
			sfx(1,1)
			spawn_dust_parts(0,
				flr(10*bullet.death_part_mul),
				bullet.pos
			)
			spawn_dust_parts(1,
				flr(2*bullet.death_part_mul),
				bullet.pos
			)
			break
		end
		
		// get distance to bullet
		local bdist=v2_mag(v2_sub(
			player.pos,
			bullet.pos
		))
		if(bdist<2) then // are we clearly inside?
			play_hit_fx()
			
			// keep the last bullet, to make it clearer how the player died
			health-=25
			if(health>0) del(bullets,bullet)
		elseif(bdist<4) then
			bullet.inside_on_prev=true
			bullet.death_part_mul=5
		elseif(bullet.inside_on_prev) then
			bullet.health=0
		else
			bullet.death_part_mul=.1
		end
	end
end

function update_player_bullets()
	for pbullet in all(player_bullets) do
		// move player bullet
		pbullet.pos=v2_add(
			pbullet.pos,
			pbullet.vel
		)

		// remove player bullet if offscreen
		if(
			pbullet.pos.x>128 or
			pbullet.pos.y>128 or
			pbullet.pos.x<0 or
			pbullet.pos.y<0
		) then
			del(player_bullets,pbullet)
		end
	end
end

function spawn_rnd_bullet(x,x_dir)
		add(bullets, {
			pos=v2(x,0),
			vel=v2(
				rnd(3)*x_dir,
				rnd(4)
			),
			max_health=100,
			health=100,
			death_part_mul=.1
		})
end

function spawn_player_bullet(r)
	if(energy<1) return
	energy-=1
	
	add(player_bullets, {
		pos=player.pos,
		vel=v2_mul(v2_rot(r),-2.5)
	})
	sfx(0,0)
end

function spawn_player_bullet_burst()
	add(loop_cos,cocreate(function()
		for i=0,4 do
			for r=0,1,.1 do
				spawn_player_bullet(r+time()%10)
			end
			yield()
		end
	end))
end

function draw_bullet(bullet)
	if( // blink if the bullet is dying
		not bullet.starving and
		bullet.health>bullet.max_health*.2 or
		flr(time()*8)%2==0
	) then
		draw_actor(bullet,2,8)
	end
end

function draw_player_bullet(pbullet)
	line(
		pbullet.pos.x,
		pbullet.pos.y,
		pbullet.pos.x-pbullet.vel.x,
		pbullet.pos.y-pbullet.vel.y,
		11
	)
end
-->8
-- dust particles

dust_parts={}

function update_dust_particles()
	for part in all(dust_parts) do
		// move particle
		part.pos=v2_mod(v2_add(
			part.pos,
			part.vel
		),128)
		
		local pdist=v2_sub(player.pos,part.pos)
		local pdist_mag=v2_mag(pdist)

		// collect the particle if needed
		if(pdist_mag<5) then
			if(part.ptype==0 and (
				energy<100
				or #dust_parts>200
			)) then
				del(dust_parts,part)
				energy=min(energy+.5,100)
			elseif(part.ptype==1 and (
				health<100
				or #dust_parts>200
			)) then
				del(dust_parts,part)
				health=min(health+1,100)
			end
		end
		
		// point velocity towards player, relative to inverse distance
		part.vel=v2_clamp(v2_add(
			v2_mul(part.vel,.9),
			v2_mul(
				v2_norm(pdist),
				2/pdist_mag
			)
		),2.5)
	end
end

function spawn_dust_parts(ptype,count,pos)
	if(count==0) return
	for i=0,count do
		add(dust_parts, {
			ptype=ptype,
			pos=pos,
			vel=v2_mul(v2_rot(rnd(10)/10),rnd(count/10))
		})
	end
end

function draw_dust_part(part)
	local col=3
	if(part.ptype==1) col=12
	pset(part.pos.x,part.pos.y,col)
end
-->8
-- enemies

enemies={}

function update_enemies()
	for enemy in all(enemies) do
		enemy.pos=v2_add(
			enemy.pos,
			enemy.vel
		)
		
		for pbullet in all(player_bullets) do
			if(v2_mag(v2_sub(
				pbullet.pos,
				enemy.pos
			)) < 4) then
				enemy.health-=25
				sfx(1,1)
				screenshake(4,2)
				del(player_bullets,pbullet)
			end
		end
		
		if(enemy.health<=0) then
			del(enemies,enemy)
			spawn_dust_parts(0,
				enemy.reward_energy,
				enemy.pos
			)
			spawn_dust_parts(1,
				enemy.reward_health,
				enemy.pos
			)
			return
		end
		
		// hurt player on touch
		local pdist=v2_sub(
			player.pos,
			enemy.pos
		)
		
		if(v2_mag(pdist) < 4) then
			if(not enemy.inside_on_prev) then
				health=max(health-enemy.damage,0)
				play_hit_fx()
				enemy.inside_on_prev=true
			end
			
			player.vel=v2_mul(
				v2_norm(pdist),
				enemy.knockback
			)
		else
			enemy.inside_on_prev=false
		end
	end
end

function spawn_enemy(etype,pos)
	add(enemies, {
		etype=etype,
		pos=pos,
		vel=v2(0,.1),
		health=750,
		reward_energy=100,
		reward_health=15,
		damage=12,
		knockback=3
	})
end

function draw_enemy(enemy)
	spr(
		0,
		enemy.pos.x-4,
		enemy.pos.y-4
	)
end
__gfx__
00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00600600001111000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00066000001111000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00066000001111000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00600600001111000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
__sfx__
011000002c92001900009000090000900009000090000900009000090000900009000090000900009000090000900009000090000900009000090000900009000090000900009000090000900009000090000900
011000001592300903009030090300903009030090300903009030090300903009030090300903009030090300903009030090300903009030090300903009030090300903009030090300903009030090300903
011000001597315b00009030090300903009030090300903009030090300903009030090300903009030090300903009030090300903009030090300903009030090300903009030090300903009030090300903
001000002c90001900009000090000900009000090000900009000090000900009000090000900009000090000900009000090000900009000090000900009000090000900009000090000900009000090000900
